// Initialize variables
let firstNum = parseInt(prompt("Provide a number:"));
let secondNum = parseInt(prompt("Provide another number:"));
let sum = firstNum + secondNum;


// function for numbers
function checkNum(num1, num2, total) {
	// checks if the user input is truly a number.
	if(isNaN(num1) || isNaN(num2)) {
		alert("Error! You must input a number.");
	}
	else if(total < 10) {
		alert("The sum of two numbers is: " + total + ".");
	}
	else if(total >= 10 && total <= 20) {
		// nested if-else statement
		// checks if which one is a greater number before subtracting the two numbers
		if(num1 < num2) {
			subtract1 = num2 - num1;
			alert("The difference of two numbers is: " + subtract1 + ".");
		}
		else if(num1 > num2) {
			subtract2 = num1 - num2;
			alert("The difference of two numbers is: " + subtract2 + ".");
		}
		else {
			alert("The difference of two numbers is 0.");
		}
	}
	else if(total >= 21 && total <= 30) {
		multiply = num1 * num2;
		alert("The product of two numbers is: " + multiply + ".");
	}
	else {
		// nested if-else statement
		// checks if which one is a greater number before dividing the two numbers.
		if(num1 < num2) {
			divide1 = num2 / num1;
			alert("The quotient of two numbers is: " + divide1 + ".");
			}
		else if(num1 > num2) {
			divide2 = num1 / num2;
			alert("The quotient of two numbers is: " + divide2 + ".");
			}
		else {
			alert("The quotient of two numbers is 1.");
		};
	}; // end of else statement
}; // end of function


// function checkNum invocation
checkNum(firstNum, secondNum, sum);


// ===============================================

// Initialize variables
let name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));


// function for name and age
function checkNameAge(name, age) {
	if(name == "" || age == "") {
		alert("Are you a time traveler?");
	}
	// checks whether the age being entered is a number or not
	else if(isNaN(age)) {
		alert("Your name is " + name + ". \nHowever, your age is NOT a number.");
	}
	else {
		alert("Your name is " + name + ". \nYour age is " + age + ".");
	}; //end of else statement
}; //end of function


// function for age
function isLegalAge(age) {
	try {
		console.log(ag + nam); //using ag instead of age and nam instead of name
	}	
	catch(error) {
		console.log(typeof error);
		console.log(error.message);
	}
	finally {
		// checks whether the age being entered is a number or not
		if(isNaN(age)) {
			alert("Your age cannot be determined as legal or not.");
		}
		else if(age <= 17) {
			alert("You are not allowed here.");
		}
		else {
			alert("You are of legal age.");
		};
	}; // end of finally statement
}; //end of function


// functions invocation
checkNameAge(name, age);
isLegalAge(age);


// switch statement for age
switch(age) {
	case 18:
		alert("You are not allowed to party.");
		break;
	case 21:
		alert("You are now part of the adult society.");
		break;
	case 65:
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you're not an alien?");
}; // end of switch statement
