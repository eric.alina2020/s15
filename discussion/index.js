// Assignment Operator (=)

let assignmentNumber = 8;


// Addition Assignment Operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

assignmentNumber += 2;
console.log(assignmentNumber); //12


// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
assignmentNumber *= 2;
assignmentNumber /= 2;


// Arithmetic Operators (+, -, *, /, %)
// PEMDAS RULE
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement
let z = 1;
// Pre-fix Incrementation
++z
console.log(z); //2

// Post-fix Incrementation
// - returns the previous value of the variable and add 1 to its actual value
z++
console.log(z); //3

console.log(z++); //3
console.log(z); //4

console.log(++z); //5 - the new value is returned immediately

// Pre-fix Decrementation and Post-fix Decrementation
console.log(--z); //4 - pre-fix decrementation
console.log(z--); //4 - post-fix decrementation
console.log(z); //3

// Type Coercion
//  - is the automatic or implicit conversion of values from one data type to another
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
// The result is a number

let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// The result is a number
// The boolean "true" is associated with the value of 1

let numF = false + 1;
console.log(numF);
console.log(typeof numF);
// The boolean "false" is associated with the value of 0.


// Comparison Operators
// (==) Equality Operator
let juan = "juan";

console.log("Equality Operator");
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == "1"); //true
console.log(0 == false); //true
console.log("juan" == "JUAN"); //false - because JS is case sensitive
console.log("juan" == juan); //true

// (===) Strict Equality Operator
console.log("Strict Equality Operator");
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false - because of different data type
console.log(0 === false); //false - because of different data type
console.log("juan" === "JUAN"); //false - because JS is case sensitive
console.log("juan" === juan); //true

// (!=) Inequality Operator
console.log("Inequality Operator");
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != "1"); //false - because of different data type
console.log(0 != false); //false - because of different data type
console.log("juan" != "JUAN"); //true
console.log("juan" != juan); //false

// (!==) Strict Inequality Operator
console.log("Strict Inequality Operator");
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log(0 !== false); //true
console.log("juan" !== "JUAN"); //true
console.log("juan" !== juan); //false

// Relational Comparison Operator
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

// Greater than (>)
console.log("Greater Than");
console.log(x > y); //false

// Less than (<)
console.log("Less Than");
console.log(y < y); //false
console.log(numString < 6000); //true - forced/type coercion to change the string to a number
console.log(numString < 1000); //false

// Greater than or Equal to
console.log("Greater than or Equal to");
console.log(w >= w) //true

// Less than or Equal to
console.log("Less than or Equal to");
console.log(y <= y) //true


// Logical Operators (&&, ||, !)

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

// Logical AND Operator (&& - Double Ampersand)
console.log("Logical AND Operator");
// return true if ALL operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 95 && isLegalAge;
console.log(authorization3); //true

// Logical OR Operator (|| - Double Pipe)
console.log("Logical OR Operator");
//  return true if atleast ONE of the operands are true.

let userLevel = 100;
let userLevel2 = 65;
let userAge = 15

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement1); //True

// Logical NOT Operator (!)
console.log("Logical NOT Operator");
// turns a boolean into the opposite value

let opposite = !isAdmin;
console.log(opposite); //true - isAdmin original value is false
console.log(!isRegistered); //false - isRegistered original value is true

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true


// Conditional Statements
// if else if and if statement

// if statement
// - executes a statement if a specified condition is true.

if(true) {
	console.log("We just ran an if condition!");
};

let numG = 5;

if(numG < 10) {
	console.log("Hello");
};

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length >= 10 && isRegistered && isAdmin) {
	console.log("Welcome to Game Online");
} else {
	console.log("You are not ready");
};

// .length - is a property of strings which determines the number of characters in the string.

// else statement
//  - executes a statement if all other conditions are false

if(userName3.length >=10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noobies Guild!");
} else {
	console.log("You are too strong to be a noob");
};

// else if statement
//  - executes a statement if previous conditions re false

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Welcome noob");
} 
else if(userLevel3 > 25) {
	console.log("You are too strong");
} 
else if(userAge3 < requiredAge) {
	console.log("You are too young to join the guild");
} 
else if(userName3.length < 10) {
	console.log("Username too short");
} 
else {
	console.log("You are not ready");
};


// if, else if, and else statement with functions
let n = 3;
console.log(typeof n);

function addNum(num1, num2) {
	// check if the numbers being passed as an arument are number types
	if(typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);
	} 
	else {
		console.log("One or both of the arguments are not numbers");
	}

};

addNum(5, "2");


// create a login function

// check if the username and password is a string type

function login(username, password) {
	if(typeof username === "string" && typeof password === "string") {
		console.log("Both arguments are string");

		/*
			Nested if-else
				- will run if the parent if statement is able to agree to accomplush its condition
			Mini-Activity

			add another condition to our nested if statement:
				- check if the username and password is at least 8 characters long.
				- alert - "Thank you for logging in"
			add an else statement which will run if both conditions were not met:
				- show an alert message that says "Credentials too short"

			Stretch Goals:
			add an else if statement that if the username is less than 8 characters:
				- show an alert message that says "Username is too short"
			add an else if statement that if the password is less than 8 characters:
				- show an alert message that says "Password too short"
			Screenshot your output
		*/
		if(username.length >= 8 && password.length >= 8) {
			console.log("Thank you for logging in");
		} 
		else if(username.length < 8) {
			console.log("Username is too short");
		} 
		else if(password.length < 8) {
			console.log("Password is too short");
		} 
		else {
			console.log("Credentials are too short");
		}

	} 
	else {
		console.log("One or both of the arguments are not string");
	}
};

login("j", "jane");


// function with return keyword

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return "Not a typhoon yet.";
	} 
	else if(windSpeed <= 61) {
		return "Tropical depression detected.";
	}
	else if(windSpeed >=62 && windSpeed <= 88) {
		return "Tropical storm detected.";
	}
	else if(windSpeed >=89 && windSpeed <= 117) {
		return "Severe tropical storm detected."
	}
	else {
		return "Typhoon detected."
	}
};

message = determineTyphoonIntensity(68);
console.log(message); //

if(message == "Tropical storm detected.") {
	console.warn(message);
};

// console.warn is a good way to print warnings in our console that could help us developers act on a certain output within our code.

// Truthy and Falsy
// false (undefined, null, "", NaN, -0)
if(0) {
	console.log("Truthy")
}


let fName = "jane";
let mName = "doe";
let lName = "smith";

console.log(fName + " " + mName + " " + lName)

// Template Literals (ES6)

console.log(`${fName} ${mName} ${lName}`)


// Ternary Operator (ES6)
/*
Syntax:
	(expression/condition) ? ifTrue : ifFalse;
	expression/condition ? ifTrue : ifFalse;	
*/

// Single Statement Execution

let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult);

5000 > 1000 ? console.log("price is over 1000") : console.log("Price is less than 1000");


// Else if with ternary operatot

let a = 7;

a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"))

// Multiple Statement Execution
let name;

function isOfLegalAge() {
	name = "John";
	return "You are of the legal age limit"
};

function isUnderAge() {
	name = "Jane";
	return "You are under the age limit"
};

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of the ternary operator in functions: ${legalAge}, ${name}`);


// Switch Statement
/*
	- can be used as an alternative to an if-else if-else statement.
	- Syntax:
		switch(expression) {
			case value1:
				statement;
				break;
			case value2:
				statement;
				break;
			.
			.
			.
			case valuen:
				statement;
				break;
			default:
				statement;
		}
	- switch statements cannot use logical operators
*/

let day = prompt("What day of the week is it today?")
console.log(day);

switch(day) {
	case "monday":
		console.log("The color of the day is red");
		break;
	case "tuesday":
		console.log("The color of the day is orange");
		break;
	case "wednesday":
		console.log("The color of the day is yellow");
		break;
	case "thursday":
		console.log("The color of the day is green");
		break;
	case "friday":
		console.log("The color of the day is blue");
		break;
	case "saturday":
		console.log("The color of the day is indigo");
		break;
	case "sunday":
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");	
};


// Try-Catch-Finally Statement


function showIntensityAlert(windSpeed) {
	try{
		// Attempt to execute a code, alerat is not a keyword
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch(error) {
		console.log(typeof error);

		console.log(error.message);
	}
	finally {
		// Continue to execute code regardless of success or failure of code execution in the try block
		alert("Intensity updates will show new alert");
	}
};

showIntensityAlert(56);

// Solution of the Activity
/*let numA = parseInt(prompt("Provide a number"));
let numB = parseInt(prompt("Provide another number"));
let total = numA + numB;
console.log(total);

if(total >= 31) {
	alert("The quotient of the two numbers is: " + (numB / numA));
}
else if(total >= 21) {
	alert("The product of the two numbers is: " + (numB * numA));
}
else if (total >= 10) {
	alert("The difference of the two numbers is: " + (numB - numA));
}
else {
	console.warn("The sum of the two numbers is: " + total);
};

let name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));

if (name === "" || name === null || age > 100 || age === null) {
    alert("Are you a time traveler?");
} else if (name !== "" && age !== ""){
    alert("Hello " + name + ". Your age is " + age);
}

function isLegalAge(age) {
    age >= 18 ? alert("You are of legal age.") : alert("You are not allowed here.");
}

isLegalAge(age);

switch (age) {
    case 18:
        alert("You are now allowed to party.")
        break;
    case 21:
        alert("You are now part of the adult society.")
        break;
    case 65:
        alert("We thank you for your contribution to society.")
        break;
    default:
        alert("Are you sure you're not an alien?");
        break;
}

try {
    isLegalag(age);
} catch (error) {
    console.warn(error.message);
} finally {
    isLegalAge(age);
}
*/